//ignore ES6 warning
const request = require('request');
const _ = require('lodash');

const getOriginalImage = function(userId) {

        let promise = new Promise(function(resolve,reject){
            request.get('https://www.instagram.com/' + userId + '/?__a=1', (err, res, body) => {
                if (err) {
                    reject(err); 
                }
                var obj = JSON.parse(body)
                if(!_.isEmpty(obj)){
                    var finalValue = obj.graphql.user.profile_pic_url_hd;
                    resolve(finalValue); 
                }else{
                    resolve(""); 
                }
                
            });

        })

       return promise;
    };
    

export default getOriginalImage;
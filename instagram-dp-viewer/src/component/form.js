import React, { useState } from 'react';
import getOriginalImage from '../service/rest-service'



function Form(props) {

    const [instaId, setInstaId] = useState("");


    function handleChange(event) {
        setInstaId(event.target.value)
        console.log(instaId);
    }


    function handleSubmit(event) {
        setInstaId(event.target.value)
        let oirginalImageUrl = "";
        getOriginalImage(instaId).then(function (result) {
            oirginalImageUrl = result;
            if (oirginalImageUrl && oirginalImageUrl.length>0) {
                props.setIsUrlAvailable(true);
                props.setOirginalImageUrl(oirginalImageUrl);
            } else {
                props.setIsUrlAvailable(false);
                props.setOirginalImageUrl("");
            }

            setInstaId("");
        })

        event.preventDefault();
    }

    return (<form name="profilePicForm">
        <input type="text" placeholder="Enter the IG id" onChange={handleChange} value={instaId} />
        <button type="submit" name="Generate" onClick={handleSubmit}>Generate</button>
    </form>);
}

export default Form;
import React, { useState } from 'react';
import Form from './form';
import Image from './image';
const _ = require('lodash');



function Container() {


    const [oirginalImageUrl, setOirginalImageUrl] = useState("");
    const [isUrlAvailable, setIsUrlAvailable] = useState(false);





    return (<div className="container">
        <Form
            setIsUrlAvailable={setIsUrlAvailable}
            setOirginalImageUrl={setOirginalImageUrl}
        />
        <Image
            oirginalImageUrl={oirginalImageUrl}
            isUrlAvailable={isUrlAvailable}
        />
    </div>
    )

}

export default Container;
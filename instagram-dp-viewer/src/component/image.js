import React, { useState } from 'react';
import getOriginalImage from '../service/rest-service'



function Image(props) {
    const imageDivStyle = {
        display: props.isUrlAvailable ? "" : "none"
    }


    return (
        <div className="imageLoader">
            <img style={imageDivStyle} src={props.oirginalImageUrl} alt="insa HD DP" />
        </div>
    )





}
export default Image;